from __future__ import unicode_literals

from django.db import models

enum = (
    ('API', 'API'),
    ('DB', 'DB')
)


class Reports(models.Model):
    name = models.CharField(max_length=200)
    


class Reports_fields_meta_data(models.Model):
    name = models.CharField(max_length=200)
    
    report = models.ForeignKey(Reports, blank=True, null=True)


class fields_data(models.Model):
    fields = models.ForeignKey(Reports_fields_meta_data, blank=True, null=True)
    date = models.DateTimeField(auto_now = True)
    value = models.TextField()
