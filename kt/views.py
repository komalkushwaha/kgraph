from django.shortcuts import render
from django.http import HttpResponse,Http404
import json
import MySQLdb
def show_field(request):
    if request.is_ajax():
    	r=request.GET['report']
    	if r=='SIVR':
    		i=2
    	else:
    		i=1
    	db=MySQLdb.connect(user='root',db='kgraph',passwd='',host='localhost')
	cursor=db.cursor()
	cursor.execute('select name from kt_reports_fields_meta_data where report_id=%s',[i]);
	items=[]
	for row in cursor.fetchall():
		items.append(row)
        
        data = json.dumps(items)
        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404
def home(request):
	return render(request,'f3.html')

	
def send_data(request):
	if request.method=='GET':
		r=request.GET['report']
		f=request.GET['field']
		sd=request.GET['startDate']
		ed=request.GET['endDate']
		return render(request,'f2.html',{'r':r,'f':f,'sd':sd,'ed':ed})
	else:
		return HttpResponse("Please enter the fields")

